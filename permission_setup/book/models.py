from django.db import models
from django.contrib.auth.models import User


class Author(models.Model):
    user = models.ForeignKey(User,
                             on_delete=models.CASCADE,
                             related_name='authors')
    name = models.CharField(max_length=255)

    def __str__(self):
        return self.user.username


class Book(models.Model):
    author = models.ForeignKey(Author,
                             on_delete=models.CASCADE,
                             related_name='books')
    title = models.CharField(max_length=255)
    description = models.TextField()


    def __str__(self):
        return self.title
