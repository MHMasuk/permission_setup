from django.contrib.auth.models import User

from rest_framework import permissions


class IsAuthor(permissions.BasePermission):
    message = "You are not an owner"

    def has_object_permission(self, request, view, obj):
        print(obj.author)
        if request.user and request.user.groups.filter(name='admin'):
            return True
        
        return False