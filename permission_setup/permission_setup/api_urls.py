from rest_framework.routers import DefaultRouter 

from book import views as book_views


router = DefaultRouter()
router.register(r'books', book_views.BookViewSet)



